#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>

int number();
int takeGuess();
bool checkGuess(int number, int guess);

int main() {
    std::srand(std::time(nullptr));
    int rNumber = number();
    while(true){
        if(checkGuess(rNumber, takeGuess())){
            break;
        }
    }
    return 0;
}

/* Making a guessing game, generates a random number, user guesses number - gets told if they are warmer or colder than
 * their last guess.
 */

int number() {
    return std::rand() % 20;
}

int takeGuess() {
    std::cout << "Guess a number: ";
    //int newGuess = 0;
    //std::cin >> newGuess;
    //return newGuess;
    std::string line;
    std::cin >> line;
    for(int i = 0; i < line.length(); i++){
        if(!isdigit(line[i])){
            std::cout << "Use a number you fucking degenerate!" << std::endl;
            return takeGuess();
        }
    }
    return std::atoi(line.c_str());
}

bool checkGuess(int number, int guess) {
    int offset = 5;
    if(guess == number){
        std::cout << "Well done! You guess the number! Sneaky bastard" << std::endl;
        return true;
    }
    else if(guess < number - offset || guess > number + offset){
        std::cout << "Cold. Try Again." << std::endl;
    }
    else if(guess == number -1 || guess == number + 1){
        std::cout << "Hot. So Close." << std::endl;
    }
    else {
        std::cout << "Warm. Try again." << std::endl;
    }
    return false;
}